namespace Composite.LightHtml.Visitor.Concrete_Visitors;

public class StyleVisitor : IVisitor
{
    public void Visit(LightElementNode node)
    {
        Console.WriteLine("Style visitor");
        node.AddCssClasses(["new-style"]);
        node.Build();
        Console.WriteLine(node.OuterHtml);
        Console.WriteLine("Style visitor end\n");
    }
}