namespace Decorator.Characters;

public class Character(string name): ICharacter
{
    private string Name { get; set; } = name;
    private List<ICharacter> Inventory { get; set; } = [];

    public void AddToInventory(ICharacter item)
    {
        Inventory.Add(item);
    }

   
    public virtual void Show()
    {
        Console.WriteLine($"Name: {Name}");
    }
}