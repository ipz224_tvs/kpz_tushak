﻿using Bridge.RenderWays;
using Bridge.ShapeDirectory;

class Program
{
    private static void Main()
    {
        Console.WriteLine();
        IRenderer vectorRenderer = new Vector();
        IRenderer rasterRenderer = new Raster();

        Shape circle1 = new Circle(vectorRenderer);
        Shape circle2 = new Circle(rasterRenderer);
        Shape square1 = new Square(vectorRenderer);
        Shape square2 = new Square(rasterRenderer);
        Shape triangle1 = new Triangle(vectorRenderer);
        Shape triangle2 = new Triangle(rasterRenderer);
        
        circle1.Draw();
        circle2.Draw();
        Console.WriteLine();
        
        square1.Draw();
        square2.Draw();
        Console.WriteLine();
        
        triangle1.Draw();
        triangle2.Draw();
        Console.WriteLine();
    }
}