using Lab_02_Abstract_Fabric.Devices.Interfaces;
using Lab_02_Abstract_Fabric.Devices.Kiaomi;

namespace Lab_02_Abstract_Fabric.Factories;

public class KiaomiFactory: ITechnologyFactory
{
    public IEBook CreateEBook()
    {
        return new KiaomiEBook();
    }

    public ILaptop CreateLaptop()
    {
        return new KiaomiLaptop();
    }

    public INetbook CreateNetbook()
    {
        return new KiaomiNetbook();
    }

    public ISmartphone CreateSmartphone()
    {
        return new KiaomiSmartphone();
    }
}