using Lab_02_Abstract_Fabric.Devices.Balaxy;
using Lab_02_Abstract_Fabric.Devices.Interfaces;

namespace Lab_02_Abstract_Fabric.Factories;

public class BalaxyFactory: ITechnologyFactory
{
    public IEBook CreateEBook()
    {
        return new BalaxyEBook();
    }

    public ILaptop CreateLaptop()
    {
        return new BalaxyLaptop();
    }

    public INetbook CreateNetbook()
    {
        return new BalaxyNetbook();
    }

    public ISmartphone CreateSmartphone()
    {
        return new BalaxySmartphone();
    }
}