namespace Composite.LightHtml.Visitor;

interface IHtmlElement
{
    void Accept(IVisitor visitor);
}