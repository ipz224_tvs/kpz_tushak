namespace Mediator;

public interface ICentre
{
    void RequestRunway(Aircraft aircraft);
    void RequestTakeOff(Aircraft aircraft);
}