using System.Drawing;

namespace Lab_02_Builder.Builders;

public interface ICharacterBuilder
{
    ICharacterBuilder SetName(string name);
    ICharacterBuilder SetClass(string className);
    ICharacterBuilder SetGender(string gender);
    ICharacterBuilder SetAge(int age);
    ICharacterBuilder SetBodyType(string bodyType);
    ICharacterBuilder SetHairColor(Color color);
    ICharacterBuilder SetEyeColor(Color color);
    ICharacterBuilder SetArmor(string armor);
    ICharacterBuilder SetWeapon(string weapon);
    ICharacterBuilder SetToDoList(Dictionary<string, bool> task);
    ICharacterBuilder SetInventoryItems(Dictionary<string, int> item);
    Character Build();
}