using System.Drawing;

namespace Lab_02_Builder.Builders;

public class EnemyBuilder: ICharacterBuilder
{
    private readonly Character _entity = new();


    public ICharacterBuilder SetName(string name)
    {
        _entity.Name = name;
        return this;
    }

    public ICharacterBuilder SetClass(string className)
    {
        _entity.Class = className;
        return this;
    }

    public ICharacterBuilder SetGender(string gender)
    {
        _entity.Gender = gender;
        return this;
    }

    public ICharacterBuilder SetAge(int age)
    {
        _entity.Age = age;
        return this;
    }

    public ICharacterBuilder SetBodyType(string bodyType)
    {
        _entity.BodyType = bodyType;
        return this;
    }

    public ICharacterBuilder SetHairColor(Color color)
    {
        _entity.HairColor = color;
        return this;
    }

    public ICharacterBuilder SetEyeColor(Color color)
    {
        _entity.EyeColor = color;
        return this;
    }

    public ICharacterBuilder SetArmor(string armor)
    {
        _entity.Armor = armor;
        return this;
    }

    public ICharacterBuilder SetWeapon(string weapon)
    {
        _entity.Weapon = weapon;
        return this;
    }

    public ICharacterBuilder SetToDoList(Dictionary<string, bool> task)
    {
        _entity.TodoList = task;
        return this;
    }

    public ICharacterBuilder SetInventoryItems(Dictionary<string, int> item)
    {
        _entity.Inventory = item;
        return this;
    }


    public Character Build()
    {
        return _entity;
    }
}