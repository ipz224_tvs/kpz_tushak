namespace Mediator
{
    public class Runway
    {
        public Guid Id { get;} = Guid.NewGuid();
        public Aircraft? IsBusyWithAircraft;
        
        public bool IsAvailable() => IsBusyWithAircraft == null;
    }
}