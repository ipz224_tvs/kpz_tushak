namespace Composite.LightHtml.State;

public class VisibleElementState: IElementState
{
    public void HandleVisibility(LightElementNode lightElementNode)
    {
        Console.ForegroundColor = ConsoleColor.Green;
        lightElementNode.AddAttributes(new Dictionary<string, string>() { { "visibility", "visible" } });
        Console.WriteLine(lightElementNode.Build().OuterHtml);
        Console.ResetColor();
    }
}