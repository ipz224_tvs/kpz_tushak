using CoR.Request;

namespace CoR.Base;

public abstract class BaseSupportHandler : ISupportHandler
{ 
    protected abstract decimal SupportCost { get; set; }
    private ISupportHandler _nextHandler = null!;
    
    public ISupportHandler SetNext(ISupportHandler handler)
    {
        _nextHandler = handler;
        return handler;
    }

    protected bool CanHandle(Request.Request request)
    {
        return SupportCost <= request.Cost;
    }
    public virtual void HandleRequest(Request.Request request)
    {
        if (CanHandle(request))
        {
            Console.WriteLine($"Request \"{request.RequestMessage}\" is handled by {this.GetType().Name}");
        }
        else
        {
            if (_nextHandler != null!)
            {
                _nextHandler.HandleRequest(request); 
            }
            else
            {
                Console.WriteLine("Request is not handled. Maybe cost is too low");
            }
        }
    }
}
