namespace Composite.LightHtml.State;

public class DisabledElementState: IElementState
{
    public void HandleVisibility(LightElementNode lightElementNode)
    {
        Console.ForegroundColor = ConsoleColor.Blue;
        lightElementNode.AddAttributes(new Dictionary<string, string>() { { "visibility", "disabled" } });
        Console.WriteLine(lightElementNode.Build().OuterHtml);
        Console.ResetColor();
    }
}