namespace CoR.Request;

public enum SupportLevel
{
    Level1,
    Level2,
    Level3,
    Level4
}
