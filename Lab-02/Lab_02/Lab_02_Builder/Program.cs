﻿using System.Drawing;
using Lab_02_Builder.Builders;

namespace Lab_02_Builder
{
    class Program
    {
        static void Main(string[] args)
        {
            var heroBuilder = new HeroBuilder();
            var enemyBuilder = new EnemyBuilder();
            
            Director director = new Director
            {
                Builder = heroBuilder
            };
            director.CreateCharacter(
                name:"Whytalik", 
                classname: "Warrior",
                gender:"Male", 
                age:20,
                hairColor: Color.Red, 
                eyeColor: Color.Blue, 
                bodyType: "Heavy", 
                armor: "Plate", 
                weapon: "Sword", 
                tasks: new Dictionary<string, bool>(){
                    {"Win a fight", false},
                    {"Get an item", false},
                    {"Heal", false}
                }, 
                inventoryItems: new Dictionary<string, int>()
                {
                    {"Pork", 3},
                    {"Carrot", 4},
                    {"Iron", 5}
                });

            Console.WriteLine("\n------------------------------------------------------------");
            var hero = heroBuilder.Build();
            Console.WriteLine(hero);
            hero.DoTask("Win a fight");

            director.Builder = enemyBuilder;
            
            director.CreateCharacter(
                name: "Computer Networks",
                classname: "Politeh",
                gender: "Male",
                age: 4,
                hairColor: Color.Red,
                eyeColor: Color.Red,
                bodyType: "Heavy",
                armor: "Iron",
                weapon: "Sword",
                tasks: new Dictionary<string, bool>()
                {
                    {"49 points", false},
                    {"Kill everyone", false},
                    {"20 point per module work", true}
                },
                inventoryItems: new Dictionary<string, int>()
                {
                    {"Labs", 16},
                    {"All points", 100},
                    {"Points per lab", 2}
                }
                );
            
            Console.WriteLine("\n------------------------------------------------------------");
            var enemy = enemyBuilder.Build();
            Console.WriteLine(enemy);
            enemy.DoTask("49 points");
        }
    }
}

