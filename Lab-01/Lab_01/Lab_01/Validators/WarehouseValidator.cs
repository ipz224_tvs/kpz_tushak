using System.IO;

namespace Lab_01.Validators;

public class WarehouseValidator: IWarehouseValidator
{
    public void ValidateQuantity(int value)
    {
        switch (value)
        {
            case < 0:
                throw new InvalidDataException("Total quantity < 0");
        }
    }
}