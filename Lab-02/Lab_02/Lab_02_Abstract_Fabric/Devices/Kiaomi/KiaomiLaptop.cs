using Lab_02_Abstract_Fabric.Devices.Interfaces;

namespace Lab_02_Abstract_Fabric.Devices.Kiaomi;

public class KiaomiLaptop : ILaptop
{
    public string GetDeviceName()
    {
        return "Kiaomi Laptop";
    }
}