namespace Composite.LightHtml.Iterator;

public interface IIterator
{
    bool HasNext();
    LightNode Next();
}