﻿// Створення HTML-елемента

using Observer.LightHtml;

var div = new LightHtmlBuilder()
    .AddTagName("div")
    .AddCssClasses(new List<string> { "class1", "class2" })
    .AddClosing(false)
    .Build();

Console.WriteLine(div.OuterHtml);


var clickDiv = new Observer.Observer("click", node =>
{
    Console.WriteLine("Click on " + node.OuterHtml);
});
div.Attach(clickDiv);
div.Notify("click");
Console.WriteLine();

var mouseOverDiv = new Observer.Observer("mouseOver", node =>
{
    Console.WriteLine("Mouse over " + node.OuterHtml);
});
div.Attach(mouseOverDiv);
div.Notify("mouseOver");
Console.WriteLine();

div.Detach(clickDiv);
div.Notify("click");
Console.WriteLine();