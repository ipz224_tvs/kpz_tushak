namespace Composite.LightHtml.State;

public interface IElementState
{
    void HandleVisibility(LightElementNode lightElementNode);
}