namespace Composite.LightHtml.Command;

public class RemoveElementCommand(LightElementNode parentElement, LightElementNode elementToRemove)
    : ICommand
{
    private LightElementNode _parentElement = parentElement;
    private LightElementNode _elementToRemove = elementToRemove;

    public void Execute()
    {
        _parentElement.Children.Remove(_elementToRemove);
    }
}