namespace Strategy;

public interface IImageLoadingStrategy
{
    Task<byte[]> LoadImage(string href);
}
