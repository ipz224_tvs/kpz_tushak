using Lab_02_Abstract_Fabric.Devices.Interfaces;

namespace Lab_02_Abstract_Fabric.Devices.Balaxy;

public class BalaxyNetbook : INetbook
{
    public string GetDeviceName()
    {
        return "Balaxy Netbook";
    }
}