using Decorator.Characters;

namespace Decorator.Decorators;

public class ArmorDecorator : BaseDecorator
{
    private string Armor {  get; set; }
    public ArmorDecorator(ICharacter character, string armor) : base(character)
    {
        Armor = armor;
    }

    public override void Show()
    {
        Character.Show();
        Console.WriteLine($"Armor: {Armor}");
    }
}