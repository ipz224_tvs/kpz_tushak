using CoR.Base;
using CoR.Request;

namespace CoR.Levels;

public class ThirdLevelSupport: BaseSupportHandler
{
    protected override decimal SupportCost { get; set; } = 2500;

    public override void HandleRequest(Request.Request request)
    {
        if (CanHandle(request))
        {
            Console.WriteLine("\n--------------------------------------------------------------------------------------------------------");
            Console.WriteLine($"Request \"{request.RequestMessage}\" with cost {request.Cost} is handled by {this.GetType().Name}");
            Console.WriteLine("---------------------------------------------------------------------------------------------------------\n");
            return;
        }
        
        base.HandleRequest(request);
    }
}