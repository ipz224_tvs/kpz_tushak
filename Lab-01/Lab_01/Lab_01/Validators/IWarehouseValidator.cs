namespace Lab_01.Validators;

public interface IWarehouseValidator
{
    public void ValidateQuantity(int value);
}