namespace CoR.Base;

public interface ISupportHandler
{
    ISupportHandler SetNext(ISupportHandler handler);
    void HandleRequest(Request.Request request);
}
