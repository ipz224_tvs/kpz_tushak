using System.IO;

namespace Lab_01.Validators;

public class MoneyValidator: IMoneyValidator
{
    public void ValidateRestPart(int value)
    {
        switch (value)
        {
            case < 0:
                throw new InvalidDataException("Total rest < 0 ");
            case > 100:
                throw new InvalidDataException($"Total rest > 100 ");
        }
    }

    public void ValidateWholePart(int value)
    {
        switch (value)
        {
            case < 0:
                throw new InvalidDataException($"Total wholePart < 0 ");
        }
    }

}