namespace Composite.LightHtml.Iterator
{
    public class BreadthFirstIterator : IIterator
    {
        private readonly Queue<LightNode> _queue = new Queue<LightNode>();

        public BreadthFirstIterator(LightNode root)
        {
            _queue.Enqueue(root);
        }

        public bool HasNext() => _queue.Count > 0;

        public LightNode Next()
        {
            if (!HasNext())
                throw new InvalidOperationException("No more elements to iterate");
            
            LightNode node = _queue.Dequeue();
            if (node is LightElementNode elementNode)
                foreach (var child in elementNode.Children)
                    _queue.Enqueue(child);
            
            return node;
        }
    }
}
