using Lab_02_Abstract_Fabric.Devices.Interfaces;

namespace Lab_02_Abstract_Fabric.Devices.Balaxy;

public class BalaxyEBook : IEBook
{
    public string GetDeviceName()
    {
        return "Balaxy EBook";
    }
}