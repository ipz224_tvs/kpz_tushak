namespace Proxy;

public class SmartTextChecker : SmartTextReader
{
    public new void ReadText(string path)
    {
        Console.WriteLine($"Opening file: {path}");

        var result = base.ReadText(path);

        if (result != null)
        {
            int rowCount = result.Length;
            int charCount = result.Sum(row => row.Length);

            Console.WriteLine($"Total rows: {rowCount}, total characters: {charCount}");
        }

        Console.WriteLine($"Closing file: {path}");
    }
}