using Lab_02_Abstract_Fabric.Devices.Interfaces;

namespace Lab_02_Abstract_Fabric.Factories;

public interface ITechnologyFactory
{
    public IEBook CreateEBook();
    public ILaptop CreateLaptop();
    public INetbook CreateNetbook();
    public ISmartphone CreateSmartphone();
}