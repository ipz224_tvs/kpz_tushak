namespace Bridge.RenderWays;

public interface IRenderer
{
    public void Render(string shape);
}