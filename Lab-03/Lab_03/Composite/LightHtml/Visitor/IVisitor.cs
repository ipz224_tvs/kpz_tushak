namespace Composite.LightHtml.Visitor;

public interface IVisitor
{
    void Visit(LightElementNode node);
}