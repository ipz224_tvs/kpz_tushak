﻿using Proxy;

class Program
{
    private static void Main()
    {
        var checker = new SmartTextChecker();
        checker.ReadText(@"C:\Users\vital\OneDrive\Робочий стіл\Course 2.2\Labs\kpz_tushak\Lab-03\Lab_03\Proxy\text.txt");
        Console.WriteLine();

        var locker = new SmartTextReaderLocker(@"restricted_\d+\.txt");
        locker.ReadText(@"C:\Users\vital\OneDrive\Робочий стіл\Course 2.2\Labs\kpz_tushak\Lab-03\Lab_03\Proxy\restricted_123.txt");
    }
}