namespace CoR.Request;

public class Request(string requestMessage, decimal cost)
{
    public string RequestMessage { get; set; } = requestMessage;
    public decimal Cost { get; set; } = cost;
}