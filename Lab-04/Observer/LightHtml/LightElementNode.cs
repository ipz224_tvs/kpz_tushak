using System;
using System.Collections.Generic;
using System.Text;

namespace Observer.LightHtml
{
    public class LightElementNode(
        string tagName,
        bool isSingle,
        List<string> cssClasses,
        Dictionary<string, string> attributes,
        List<LightNode> children)
        : LightNode, ISubject
    {
        private readonly List<IObserver> _observers = new List<IObserver>();

        public string TagName { get; } = tagName;
        public bool IsSingle { get; } = isSingle;
        public List<string> CssClasses { get; } = cssClasses;
        public Dictionary<string, string> Attributes { get; } = attributes;
        public List<LightNode> Children { get; } = children;

        public override string OuterHtml
        {
            get
            {
                var html = new StringBuilder();
                html.Append($"<{TagName}");

                if (CssClasses.Count > 0)
                {
                    html.Append($" class=\"{string.Join(" ", CssClasses)}\"");
                }

                html.Append('>');

                if (Attributes.Count > 0)
                {
                    foreach (var attribute in Attributes)
                    {
                        html.Append($" {attribute.Key}=\"{attribute.Value}\"");
                    }
                }
                
                if (Children.Count > 0 )
                {
                    foreach (var child in Children)
                    {
                        html.Append(child.OuterHtml);
                    }
                }

                switch (IsSingle)
                {
                    case false:
                        html.Append($"</{TagName}>");
                        break;
                    case true:
                        html.Append("/>");
                        break;
                }

                return html.ToString();
            }
        }

        public override string InnerHtml
        {
            get
            {
                var html = new StringBuilder();

                foreach (var child in Children)
                {
                    html.Append(child.OuterHtml);
                }

                return html.ToString();
            }
        }

        public void Attach(IObserver observer)
        {
            _observers.Add(observer);
        }

        public void Detach(IObserver observer)
        {
            _observers.Remove(observer);
        }

        public void Notify(string message)
        {
            foreach (var observer in _observers)
            {
                observer.Update(this);
            }
        }
    }
}
