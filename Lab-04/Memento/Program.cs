﻿using Memento;

var initialDocument = new TextDocument("Hello, world!", ["Initial content", "Second line", "Third line"]);
var editor = new TextEditor( initialDocument);
editor.SaveDocumentState();
Console.WriteLine("Document content: " +  initialDocument);

initialDocument.Content = ["New content"];
editor.SaveDocumentState();
Console.WriteLine("Document content: " +  initialDocument);


var undoResult = editor.UndoLastChange();
if (undoResult != null)
{
    Console.WriteLine("Undo successful. Document content: " + undoResult);
}
else
{
    Console.WriteLine("Undo not possible.");
}


var redoResult = editor.RedoLastChange();
if (redoResult != null)
{
    Console.WriteLine("Redo successful. Document content: " + redoResult);
}
else
{
    Console.WriteLine("Redo not possible.");
}