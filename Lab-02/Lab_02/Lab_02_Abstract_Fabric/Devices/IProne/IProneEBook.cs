using Lab_02_Abstract_Fabric.Devices.Interfaces;

namespace Lab_02_Abstract_Fabric.Devices.IProne;

public class IProneEBook: IEBook
{
    public string GetDeviceName()
    {
        return "IProne EBook";
    }
    
}