namespace Lab_02_Fabric_Method.Subscriptions;

public interface ISubscription
{
    public void GetSubscriptionInfo();
}