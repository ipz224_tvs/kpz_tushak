namespace Memento
{
    public class TextEditor
    {
        public TextDocument Document { get; set; }
        private readonly List<IDocumentMemento> _mementos = new();
        private int _currentMementoIndex = -1;

        public TextEditor(TextDocument document)
        {
            Document = document;
        }

        public void SaveDocumentState()
        {
            _mementos.Add(Document.CreateMemento());
            _currentMementoIndex = _mementos.Count - 1;
        }
        
        public void RestoreDocumentState(IDocumentMemento memento)
        {
            Document.RestoreMemento(memento);
        }
        
        public TextDocument? UndoLastChange()
        {
            if (_currentMementoIndex > 0 && _currentMementoIndex < _mementos.Count)
            {
                _currentMementoIndex--;
                Document.RestoreMemento(_mementos[_currentMementoIndex]);
                return Document;
            }
            
            return null;
        }
        
        public TextDocument? RedoLastChange()
        {
            if (_currentMementoIndex >= 0 && _currentMementoIndex < _mementos.Count - 1)
            {
                _currentMementoIndex++;
                Document.RestoreMemento(_mementos[_currentMementoIndex]);
                return Document;
            }
            
            return null;
        }
    }
}