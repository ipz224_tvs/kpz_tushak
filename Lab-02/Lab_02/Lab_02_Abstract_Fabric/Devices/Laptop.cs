using Lab_02_Abstract_Fabric.Devices.Interfaces;

namespace Lab_02_Abstract_Fabric.Devices;

public class Laptop: ILaptop
{
    public void TurnOn()
    {
        throw new NotImplementedException();
    }

    public void TurnOff()
    {
        throw new NotImplementedException();
    }

    public void OpenLid()
    {
        throw new NotImplementedException();
    }

    public void CloseLid()
    {
        throw new NotImplementedException();
    }
}