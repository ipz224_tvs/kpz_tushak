using System.Collections.Generic;
using System.Text;

namespace Observer.LightHtml
{
    public class LightHtmlBuilder : IBuilder
    {
        private string _tagName = "";
        private string _displayType = "";
        private bool _isSingle = false;
        private List<string> _cssClasses = [];
        private Dictionary<string, string> _attributes = new();
        private List<LightNode> _children = [];

        public IBuilder AddTagName(string tagName)
        {
            _tagName = tagName;
            return this;
        }

        public IBuilder AddDisplayType(string displayType)
        {
            _displayType = displayType;
            return this;
        }

        public IBuilder AddClosing(bool isSingle)
        {
            _isSingle = isSingle;
            return this;
        }

        public IBuilder AddCssClasses(List<string> cssClasses)
        {
            _cssClasses = cssClasses;
            return this;
        }

        public IBuilder AddAttributes(Dictionary<string, string> attributes)
        {
            _attributes = attributes;
            return this;
        }

        public IBuilder AddChildren(List<LightNode> children)
        {
            _children = children;
            return this;
        }

        public LightElementNode Build()
        {
            return new LightElementNode(_tagName, _isSingle, _cssClasses, _attributes, _children);
        }
    }
}