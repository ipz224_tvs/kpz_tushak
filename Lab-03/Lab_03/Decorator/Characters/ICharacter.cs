namespace Decorator.Characters;

public interface ICharacter
{
    public void Show();
}