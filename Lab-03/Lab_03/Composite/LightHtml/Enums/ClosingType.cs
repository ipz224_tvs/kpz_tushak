namespace Composite.LightHtml.Enums;

public enum ClosingType
{
    Single,
    Double
}