﻿using CoR.Levels;
using CoR.Request;

var request1 = new Request( "I need help with my homework level 1", 1000);
var request2 = new Request("I need help with my homework level 2", 2500);
var request3 = new Request( "I need help with my homework level 3", 5000);
var request4 = new Request( "I need help with my homework level 4", 10000);


var firstLevelSupport = new FirstLevelSupport();
var secondLevelSupport = new SecondLevelSupport();
var thirdLevelSupport = new ThirdLevelSupport();
var fourthLevelSupport = new FourthLevelSupport();

firstLevelSupport
    .SetNext(secondLevelSupport)
    .SetNext(thirdLevelSupport)
    .SetNext(fourthLevelSupport);

firstLevelSupport.HandleRequest(request1);
firstLevelSupport.HandleRequest(request2);
firstLevelSupport.HandleRequest(request3);
firstLevelSupport.HandleRequest(request4);