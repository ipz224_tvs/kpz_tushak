namespace Lab_02_Fabric_Method.Subscriptions;

public class EducationalSubscription: ISubscription
{
    private decimal _monthlyFee;
    private List<string> _channels;
    private int _minimumSubscriptionPeriod;

    private string Name { get; set; }

    private decimal MonthlyFee
    {
        get => _monthlyFee;
        set
        {
            if (value < 0)
                throw new ArgumentException("Monthly fee cannot be negative.");
            _monthlyFee = value;
        }
    }

    private List<string> Channels
    {
        get => _channels;
        set => _channels = value ?? [];
    }

    private int MinimumSubscriptionPeriod
    {
        get => _minimumSubscriptionPeriod;
        set
        {
            if (value <= 0)
                throw new ArgumentException("Minimum subscription period must be a positive integer.");
            _minimumSubscriptionPeriod = value;
        }
    }
    
    public EducationalSubscription(string name, decimal monthlyFee, List<string> channels, int minimumSubscriptionPeriod)
    {
        Name = name;
        MonthlyFee = monthlyFee;
        Channels = channels;
        MinimumSubscriptionPeriod = minimumSubscriptionPeriod;
    }
    
    public void GetSubscriptionInfo()
    {
        Console.WriteLine($"Name: {Name}");
        Console.WriteLine($"Monthly fee: {MonthlyFee}");
        Console.WriteLine($"Chanel: {string.Join(", ", Channels)}");
        Console.WriteLine($"Minimum subscription period: {MinimumSubscriptionPeriod} months");
    }
}