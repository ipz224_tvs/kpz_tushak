﻿namespace Lab_02_Prototype
{
    internal static class Program
    {
        private static void Main()
        {
            //Creating generations
            Virus firstGeneration = new Virus(1.5, 5, "First Generation", "VirusFirstType");
            Virus secondGeneration = new Virus(1.3, 3, "Second Generation ", "VirusSecondType");
            Virus thirdGenerationOne = new Virus(1.1, 1, "Third Generation One", "VirusThirdType");
            Virus thirdGenerationTwo = new Virus(1.2, 2, "Third Generation Two", "VirusFourthType");
            
            //Adding children
            firstGeneration.AddChild(secondGeneration);
            secondGeneration.AddChild(thirdGenerationOne);
            secondGeneration.AddChild(thirdGenerationTwo);
            firstGeneration.AddChild(secondGeneration);
            
            //Cloning
            var cloned = (Virus)firstGeneration.Clone();
            Console.WriteLine("Cloned Parent:");
            Console.WriteLine(cloned);
            foreach (var child in cloned._children)
            {
                Console.WriteLine($"Child: {child}");
            }
        }
    }
}

