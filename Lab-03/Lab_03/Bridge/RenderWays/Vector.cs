using Bridge.ShapeDirectory;

namespace Bridge.RenderWays;

public class Vector: IRenderer
{
    public void Render(string shape)
    {
        Console.WriteLine($"Drawing {shape} as vector graphics");
    }
}