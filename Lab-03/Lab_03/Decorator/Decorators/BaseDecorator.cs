using Decorator.Characters;

namespace Decorator.Decorators;

public abstract class BaseDecorator : ICharacter
{
    protected readonly ICharacter Character;

    protected BaseDecorator(ICharacter character)
    {
        Character = character;
    }

    public abstract void Show();
}