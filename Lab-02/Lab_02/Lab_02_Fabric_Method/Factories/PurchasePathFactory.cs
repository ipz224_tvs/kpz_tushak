using Lab_02.Subscriptions;

namespace Lab_02.Factories;

public abstract class PurchasePathFactory
{
    public abstract ISubscription CreateSubscription(string name);
}