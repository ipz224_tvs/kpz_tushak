namespace Mediator;

public class CommandCentre : ICentre
{
    private List<Aircraft> _aircrafts = [];
    private List<Runway> _runways = [];
    
    public void AddRunway(Runway runway)
    {
        _runways.Add(runway);
    }
    
    public void RequestRunway(Aircraft aircraft)
    {
        Console.WriteLine("Aircraft " + aircraft.Name + " requested a runway");
        if (_runways.Any(r => r.IsBusyWithAircraft != aircraft ))
        {
            if (aircraft.CurrentRunway == null)
            {
                var availableRunway = _runways.FirstOrDefault(r => r.IsAvailable());
                if (availableRunway != null)
                {
                    availableRunway.IsBusyWithAircraft = aircraft;
                    aircraft.CurrentRunway = availableRunway;
                    aircraft.Land();
                    return;
                }

                Console.WriteLine("All runways are busy");
                return;
            }
            
            Console.WriteLine("Aircraft " + aircraft.Name + " is already on a runway");
        }
    }

    public void RequestTakeOff(Aircraft aircraft)
    {
        Console.WriteLine("Aircraft " + aircraft.Name + " requested take off");

        var runway = aircraft.CurrentRunway;
        
        if (runway != null)
        {
            if (runway.IsBusyWithAircraft == aircraft)
            {
                Console.WriteLine("Aircraft is taking off from its current runway");
                runway.IsBusyWithAircraft = null;
                aircraft.CurrentRunway = null;
                aircraft.TakeOff();
                return;
            }
            
            Console.WriteLine("Aircraft " + aircraft.Name + " is not on a runway");
        }
        
        Console.WriteLine("Aircraft " + aircraft.Name + " doesn't have a runway");
    }
}