﻿using Strategy;

class Program
{
    static async Task Main(string[] args)
    {
        var networkHref = "https://cdn.pixabay.com/photo/2015/04/19/08/32/marguerite-729510_640.jpg";
        var fileSystemHref = "C:\\Users\\vital\\OneDrive\\Робочий стіл\\Course 2.2\\Labs\\kpz_tushak\\Lab-04\\Strategy\\image.jpg";

        var networkImage = new LightImage(new NetworkImageLoadingStrategy(), networkHref);
        var fileSystemImage = new LightImage(new FileSystemImageLoadingStrategy(), fileSystemHref);
        
        byte[] networkImageData = await networkImage.Load();
        byte[] fileSystemImageData = await fileSystemImage.Load();
        
        await File.WriteAllBytesAsync(Path.Combine("C:\\Users\\vital\\OneDrive\\Робочий стіл\\Course 2.2\\Labs\\kpz_tushak\\Lab-04\\Strategy\\", "network_image.png"), networkImageData);
        await File.WriteAllBytesAsync(Path.Combine("C:\\Users\\vital\\OneDrive\\Робочий стіл\\Course 2.2\\Labs\\kpz_tushak\\Lab-04\\Strategy\\", "file_system_image.png"), fileSystemImageData);

        Console.WriteLine($"Network image data length: {networkImageData.Length}");
        Console.WriteLine($"File system image data length: {fileSystemImageData.Length}");
    }
}