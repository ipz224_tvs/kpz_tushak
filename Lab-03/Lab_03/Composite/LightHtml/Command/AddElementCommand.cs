namespace Composite.LightHtml.Command;

public class AddElementCommand(LightElementNode parentElement, LightElementNode elementToAdd)
    : ICommand
{
    private LightElementNode _parentElement = parentElement;
    private LightElementNode _elementToAdd = elementToAdd;

    public void Execute()
    {
        _parentElement.Children.Add(_elementToAdd);
    }
}