using Lab_02_Fabric_Method.Subscriptions;

namespace Lab_02_Fabric_Method.Factories;

public class ManagerCallFactory: IPurchasePath
{
    public ISubscription CreateSubscription(string name)
    {
        Console.WriteLine("\n---------------------------------------");
        Console.WriteLine(" Buy subscription by Manager Call");
        return name switch
        {
            "D" => new DomesticSubscription("Domestic Subscription", 300,
                ["Domestic channel1", "Domestic channel2"], 3),
            
            "E" => new EducationalSubscription("Educational Subscription", 500, 
                ["Educational channel1", "Educational channel2"], 6),
            
            "F" => new PremiumSubscription("Premium Subscription", 1000,
                ["Premium channel1", "Premium channel2"], 12),
            
            _ => throw new ArgumentException("Invalid subscription type")
        };
    }
}