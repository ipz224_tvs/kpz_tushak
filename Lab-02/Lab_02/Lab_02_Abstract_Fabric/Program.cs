﻿using Lab_02_Abstract_Fabric.Factories;

namespace Lab_02_Abstract_Fabric
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\n > Balaxy Factory");
            Console.WriteLine("------------------------------------");
            ClientMethod(new BalaxyFactory());
            
            
            
            Console.WriteLine("\n > IProne Factory");
            Console.WriteLine("------------------------------------");
            ClientMethod(new IProneFactory());
            
            
            Console.WriteLine("\n > Kiaomi Factory");
            Console.WriteLine("------------------------------------");
            ClientMethod(new KiaomiFactory());
        }

        private static void ClientMethod(ITechnologyFactory factory)
        {
            var laptop = factory.CreateLaptop();
            var netbook = factory.CreateNetbook();
            var smartphone = factory.CreateSmartphone();
            var ebook = factory.CreateEBook();
            
            Console.WriteLine(">>> " + laptop.GetDeviceName());
            Console.WriteLine(">>> " + netbook.GetDeviceName());
            Console.WriteLine(">>> " + smartphone.GetDeviceName());
            Console.WriteLine(">>> " + ebook.GetDeviceName());
            Console.WriteLine("\n------------------------------------");
        }
    }
}