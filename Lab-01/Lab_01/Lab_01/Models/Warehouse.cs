using System;
using Lab_01.Validators;

namespace Lab_01.Models;

public class Warehouse
{
    private IWarehouseValidator _warehouseValidator;
    public Product _product;
    private int _quantity;
    public int Quantity
    {
        get => _quantity;
        set
        {
            _warehouseValidator.ValidateQuantity(value);
            _quantity = value;
        }
    }
    public DateTime LastImportDate { get; set; }
    
    public Warehouse(Product product, int quantity, DateTime lastImportDate, IWarehouseValidator warehouseValidator)
    {
        _product = product;
        _warehouseValidator = warehouseValidator;
        Quantity = quantity;
        LastImportDate = lastImportDate;
    }
    
    public void BuyProduct(int quantity)
    {
        Quantity -= quantity;
    }
    
    public void ImportProduct(int quantity)
    {
        Quantity += quantity;
        LastImportDate = DateTime.Now;
    }
}