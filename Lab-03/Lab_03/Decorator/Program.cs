﻿using Decorator.Characters;
using Decorator.Decorators;

namespace Decorator;

class Program
{
    private static void Main()
    {
        ICharacter warrior = new Warrior("Aragorn");
        warrior = new ArtifactDecorator(warrior, "Excalibur");
        warrior = new ArmorDecorator(warrior, "Chainmail");
        warrior.Show();
        Console.WriteLine();

        ICharacter mage = new Mage("Merlin");
        mage = new ArmorDecorator(mage, "Pointy hat");
        mage = new WeaponDecorator(mage, "Magic staff");
        mage = new WeaponDecorator(mage, "Wand of Power");
        mage.Show();
        Console.WriteLine();
        
        ICharacter paladin = new Palladin("Lancelot");
        paladin = new WeaponDecorator(paladin, "Holy sword");
        paladin = new ArmorDecorator(paladin, "Plate armor");
        paladin = new ArtifactDecorator(paladin, "Holy Grail");
        paladin.Show();
        Console.WriteLine();
    }
}