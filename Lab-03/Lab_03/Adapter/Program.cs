﻿using System;

namespace Adapter
{
    internal class Program
    {
        private static void Main()
        {
            const string filePath = @"C:\Users\vital\OneDrive\Робочий стіл\Course 2.2\Labs\kpz_tushak\Lab-03\Lab_03\Adapter\log.txt";
            ILogger logger = new Logger();

            logger.Log("Console Log message.");
            logger.Error("Console Error message.");
            logger.Warn("Console Warn message.");

            FileWriter fileWriter = new FileWriter(filePath);
            FileWriterAdapter fileWriterAdapter = new FileWriterAdapter(fileWriter);
            fileWriterAdapter.Log("Log message");
            fileWriterAdapter.Error("Error message");
            fileWriterAdapter.Warn("Warn message");
        }
    }
}