﻿using Lab_01.Models;
using Lab_01.Validators;

class Program
{
    static void Main()
    {
        MoneyValidator moneyValidator = new MoneyValidator();
        Money money = new Money(10, 50, Currencies.USD, moneyValidator);
        Console.WriteLine($"Money: {money}");
        Console.WriteLine("-------------------------------");
        
        Product product = new Product("Comod Community Course", money, "units");
        Console.WriteLine($"Product Name: {product.Name}");
        Console.WriteLine($"Product Price: {product.Price}");
        Console.WriteLine($"Measurement Unit: {product.MeasurementUnit}");
        Console.WriteLine("-------------------------------");
        product.ReducePrice(2.25m);
        Console.WriteLine($"Product Price: {product.Price}");
        Console.WriteLine("-------------------------------");

        Warehouse warehouse = new Warehouse(product, 100, DateTime.Now, new WarehouseValidator());
        Reporting reporting = new Reporting(warehouse);
        Console.WriteLine("-------------------------------");
        Console.WriteLine(reporting.PrintInventoryReport(warehouse));
        Console.WriteLine("-------------------------------\n");
        
        Console.WriteLine("-------------------------------");
        reporting.RegisterIncome(30);
        Console.WriteLine(reporting.PrintIncomeInvoices());
        Console.WriteLine("-------------------------------\n");
        
        Console.WriteLine("-------------------------------");
        reporting.RegisterExpense(40);
        Console.WriteLine(reporting.PrintExpenseInvoices());
        Console.WriteLine("-------------------------------\n");
        
        Console.WriteLine("-------------------------------");
        Console.WriteLine(reporting.PrintInventoryReport(warehouse));
        Console.WriteLine("-------------------------------\n");
    }
}