using Lab_02_Abstract_Fabric.Devices.Interfaces;

namespace Lab_02_Abstract_Fabric.Devices.IProne;

public class IProneLaptop : ILaptop
{
    public string GetDeviceName()
    {
        return "IProne Laptop";
    }
}