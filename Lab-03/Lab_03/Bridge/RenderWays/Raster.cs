using Bridge.ShapeDirectory;

namespace Bridge.RenderWays;

public class Raster: IRenderer
{
    public void Render(string shape)
    {
        Console.WriteLine($"Drawing {shape} as raster graphics");
    }
}