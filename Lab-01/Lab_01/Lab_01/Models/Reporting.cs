namespace Lab_01.Models
{
    public class Reporting
    {
        private readonly List<string> _incomeInvoices;
        private readonly List<string> _expenseInvoices;
        private Warehouse _warehouse;
        private Product _product;

        public Reporting(Warehouse warehouse)
        {
            _incomeInvoices = new List<string>();
            _expenseInvoices = new List<string>();
            _warehouse = warehouse;
        }

        public void RegisterIncome(int quantity)
        {
            _warehouse.ImportProduct(quantity);
            string invoice = CreateInvoice("Income", quantity, quantity * _warehouse._product.Price.GetTotalMoney());
            _incomeInvoices.Add(invoice);
        }

        public void RegisterExpense(int quantity)
        {
            _warehouse.BuyProduct(quantity);
            string invoice = CreateInvoice("Expense", quantity, quantity * _warehouse._product.Price.GetTotalMoney());
            _expenseInvoices.Add(invoice);
        }

        public string PrintInventoryReport(Warehouse warehouse)
        {
            return $"Inventory Report - {DateTime.Now}\n" +
                   $"Last Import Date: {warehouse.LastImportDate}\n" +
                   $"Current Quantity: {warehouse.Quantity}";
        }

        public string PrintIncomeInvoices()
        {
            return PrintInvoices("Income Invoices", _incomeInvoices);
        }

        public string PrintExpenseInvoices()
        {
            return PrintInvoices("Expense Invoices", _expenseInvoices);
        }

        private string CreateInvoice(string type, int quantity, decimal totalAmount)
        {
            return $"{type} Invoice: {quantity} units for {totalAmount} at {DateTime.Now}";
        }

        private string PrintInvoices(string title, List<string> invoices)
        {
            return $"{title}:\n" + string.Join("\n", invoices);
        }
    }
}