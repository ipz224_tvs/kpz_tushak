using Decorator.Characters;

namespace Decorator.Decorators;

public class ArtifactDecorator: BaseDecorator
{
    private string Artifact { get; set; }
    public ArtifactDecorator(ICharacter character, string artifact) : base(character)
    {
        Artifact = artifact;
    }

    public override void Show()
    {
        Character.Show();
        Console.WriteLine($"Artifact: {Artifact}");
    }
}