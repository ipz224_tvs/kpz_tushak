using Bridge.RenderWays;

namespace Bridge.ShapeDirectory;

public class Circle: Shape
{
    public Circle(IRenderer renderer) : base(renderer)
    {
    }

    public override void Draw()
    {
        Renderer.Render("Circle");
    }
}