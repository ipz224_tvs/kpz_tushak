using Decorator.Characters;

namespace Decorator.Decorators;

public class WeaponDecorator: BaseDecorator
{
    private readonly string _weapon;
    public WeaponDecorator(ICharacter character, string weapon) : base(character)
    {
        _weapon = weapon;
    }

    public override void Show()
    {
        Character.Show();
        Console.WriteLine($"Weapon: {_weapon}");
    }
}