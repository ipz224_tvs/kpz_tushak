using Bridge.RenderWays;

namespace Bridge.ShapeDirectory;

public class Triangle: Shape
{
    public Triangle(IRenderer renderer) : base(renderer)
    {
    }

    public override void Draw()
    {
        Renderer.Render("Triangle");
    }
}