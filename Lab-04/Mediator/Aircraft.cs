namespace Mediator;

public class Aircraft(string name)
{
    public string Name { get; set; } = name;
    public Runway? CurrentRunway { get; set; }
    public bool IsTakingOff { get; set; }
    
    public void Land()
    {
        IsTakingOff = false;
        Console.WriteLine($"Aircraft {Name} has landed.");
    }

    public void TakeOff()
    {
        IsTakingOff = true;
        Console.WriteLine($"Aircraft {Name} has took off.");
    }
}