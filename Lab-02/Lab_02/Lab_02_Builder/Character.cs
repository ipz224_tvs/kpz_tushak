using System.Drawing;

namespace Lab_02_Builder;

public class Character
{
    public string Name { get; set; }
    public string Class { get; set; }
    public string Gender { get; set; }
    public int Age { get; set; }
    public string BodyType { get; set; }
    public Color HairColor { get; set; }
    public Color EyeColor { get; set; }
    public string Armor { get; set; }
    public string Weapon { get; set; }
    public Dictionary<string, int> Inventory { get; set; }
    public Dictionary<string, bool> TodoList { get; set; }

    public override string ToString()
    {
        return $"Name - {Name}, Class - {Class}, Gender - {Gender}, Age - {Age}\n" +
               $"BodyType - {BodyType}, HairColor - {HairColor.Name}\n" +
               $"EyeColor - {EyeColor.Name}, Armor - {Armor}, Weapon - {Weapon}, {GetInventory()}" +
               $"{GetTodoList()}";
    }
    
    private string GetInventory()
    {
        return "\n-----------------------------------------------\n Inventory - " + 
               "\n     " + string.Join("\n     ", Inventory.Select(x => $"{x.Key} - {x.Value}"));
    }
    
    private string GetTodoList()
    {
        return "\n----------------------------------------------- \n TodoList - " + 
            "\n     " + string.Join("\n     ", TodoList.Select(x => $"{x.Key} - {x.Value}"));
    }
    
    public void DoTask(string task)
    {
        if (TodoList.ContainsKey(task))
        {
            TodoList[task] = true;
            Console.WriteLine($"{task} - done");
        }
        else
        {
            Console.WriteLine($"{task} - not found");
        }
    }
}