namespace Composite.LightHtml.State;

public class HiddenElementState: IElementState
{
    public void HandleVisibility(LightElementNode lightElementNode)
    {
        Console.ForegroundColor = ConsoleColor.Red;
        lightElementNode.AddAttributes(new Dictionary<string, string>() { { "visibility", "hidden" } });
        Console.WriteLine(lightElementNode.Build().OuterHtml);
        Console.ResetColor();
    }
}