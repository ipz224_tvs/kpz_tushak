namespace Adapter;

public class FileWriterAdapter(FileWriter fileWriter) : ILogger
{
    public void Error(string message)
    {
        fileWriter.Write($"FileWrite {message}");
        fileWriter.WriteLine($"FileWriteLine {message}");
    }

    public void Log(string message)
    {
        fileWriter.Write($"FileWrite {message}");
        fileWriter.WriteLine($"FileWriteLine {message}");
    }

    public void Warn(string message)
    {
        fileWriter.Write($"FileWrite {message}");
        fileWriter.WriteLine($"FileWriteLine {message}");
    }
}