﻿using Lab_02_Singleton.Singleton;

namespace Lab_02_Singleton
{
    class Program
    {
        public static void Main(string[] args)
        {
            Authenticator authenticator1 = Authenticator.GetInstance();
            Authenticator authenticator2 = Authenticator.GetInstance();

            // Will output True because both objects refer to the same instance of the Authenticator class
            Console.WriteLine(authenticator1 == authenticator2);

            // Check inheritance
            DerivedAuthenticator derivedAuthenticator1 = new DerivedAuthenticator();
            DerivedAuthenticator derivedAuthenticator2 = new DerivedAuthenticator();

            // Will output False because child classes do not affect a single instance of the base class
            Console.WriteLine(derivedAuthenticator1 == derivedAuthenticator2);
        }
    }
}

