namespace Task1_Library_Var;

public class Product: ErrorNotifier
{
    public string Name { get; private set; }
    private double Price { get; set; }
    

    public Product(string name, double price)
    {
        Name = name;
        Price = price;
    }
    
    public void ReducePrice(double amount)
    {
        if (IsValidReduction(amount))
        {
            Price -= amount;
            NotifyError($"Price reduced by {amount}. New price: {Price}");
        }
        else
        {
            NotifyError("Invalid reduction amount");
        }
    }

    private bool IsValidReduction(double amount)
    {
        return amount > 0 && Price - amount >= 0;
    }
}