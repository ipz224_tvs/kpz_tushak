namespace Task1_Library_Var;

public class Reporting
{
    private List<Warehouse> _warehouses;

    public Reporting()
    {
        _warehouses = new List<Warehouse>();
    }

    public void RegisterIncomingProduct(string productName, string unit, double unitPrice, int quantity, DateTime replenishmentDate)
    {
        Warehouse warehouse = new Warehouse(productName, unit, unitPrice, quantity, replenishmentDate);
        _warehouses.Add(warehouse);
        Console.WriteLine($"Incoming product registered: {productName}");
    }

    public void RegisterOutgoingProduct(string productName, int quantity)
    {
        Warehouse warehouse = _warehouses.Find(w => w.ProductName == productName);

        if (warehouse != null && IsValidOutgoing(quantity, warehouse.Quantity))
        {
            warehouse.Quantity -= quantity;
            Console.WriteLine($"Outgoing product registered: {productName}, Quantity: {quantity}");
        }
        else
        {
            Console.WriteLine($"Invalid outgoing quantity for product: {productName}");
        }
    }

    public void GenerateInventoryReport()
    {
        Console.WriteLine("Inventory Report:");
        foreach (var warehouse in _warehouses)
        {
            Console.WriteLine($"Product: {warehouse.ProductName}, Quantity: {warehouse.Quantity}");
        }
    }

    private bool IsValidOutgoing(int quantity, int availableQuantity)
    {
        return quantity > 0 && quantity <= availableQuantity;
    }
}