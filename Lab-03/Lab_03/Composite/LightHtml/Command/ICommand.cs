namespace Composite.LightHtml.Command;

public interface ICommand
{
    void Execute();
}