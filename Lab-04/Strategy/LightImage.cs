using Observer.LightHtml;

namespace Strategy;

public class LightImage(IImageLoadingStrategy loadingStrategy, string href)
    : LightElementNode("img", true, [], new Dictionary<string, string> { { "src", href }, { "alt", "image" } }, new List<LightNode>())
{
    public async Task<byte[]> Load()
    {
        return await loadingStrategy.LoadImage(href);
    }
}
