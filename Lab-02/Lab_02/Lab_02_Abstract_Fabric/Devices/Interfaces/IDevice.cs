namespace Lab_02_Abstract_Fabric.Devices.Interfaces;

public interface IDevice
{
    public string GetDeviceName();
}