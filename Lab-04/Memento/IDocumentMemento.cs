namespace Memento;

public interface IDocumentMemento
{
    public string Title { get; set; }
    public List<string> Content { get; set; }
    public DateTime Date { get; set; }
}