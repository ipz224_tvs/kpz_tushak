namespace Lab_02_Singleton.Singleton;

public class Authenticator
{
    private static Authenticator? _instance;
    protected Authenticator() { }
    
    public static Authenticator GetInstance()
    {
        return _instance ??= new Authenticator();
    }
}