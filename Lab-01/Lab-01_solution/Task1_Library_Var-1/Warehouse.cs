namespace Task1_Library_Var;

public class Warehouse: ErrorNotifier
{
    private string _productName;
    private string _unit;
    private double _unitPrice;
    private int _quantity;
    private DateTime _lastReplenishmentDate;

    public Warehouse(string productName, string unit, double unitPrice, int quantity, DateTime lastReplenishmentDate)
    {
        _productName = productName;
        _unit = unit;
        _unitPrice = unitPrice;
        _quantity = quantity;
        _lastReplenishmentDate = lastReplenishmentDate;
    }

    public void DisplayProductInfo()
    {
        NotifyError($"Product: {_productName},\n" +
                    $"Unit: {_unit},\n" +
                    $"Unit Price: {_unitPrice},\n" +
                    $"Quantity: {_quantity},\n" +
                    $"Last Replenishment Date: {_lastReplenishmentDate}");
    }
}