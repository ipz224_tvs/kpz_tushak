namespace Lab_01.Validators;

public interface IMoneyValidator
{
    public void ValidateRestPart(int value);
    public void ValidateWholePart(int value);
}