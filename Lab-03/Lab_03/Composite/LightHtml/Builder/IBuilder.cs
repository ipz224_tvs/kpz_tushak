using Composite.LightHtml.Enums;

namespace Composite.LightHtml.Builder;

public interface IBuilder
{
    IBuilder AddTagName(string tagName);
    IBuilder AddDisplayType(DisplayType displayType);
    IBuilder AddAttributes(Dictionary<string, string> attributes);
    IBuilder AddCssClasses(List<string> cssClasses);
    IBuilder AddClosingType(ClosingType closingType);
    IBuilder AddChildren(List<LightNode> children);
    LightElementNode Build();
}