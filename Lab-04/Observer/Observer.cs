using Observer.LightHtml;

namespace Observer;

public class Observer(string eventName, Action<LightNode> htmlEventDelegate) : IObserver
{
    private Action<LightNode> Action { get; } = htmlEventDelegate;
    public string EventName { get; } = eventName;

    public void Update(LightNode node)
    {
        Action.Invoke(node);
    }
}