namespace Task1_Library_Var;

public class ErrorNotifier
{
    private event EventHandler<string>? ErrorEventHandler;

    protected void NotifyError(string message)
    {
        ErrorEventHandler?.Invoke(this, message);
    }
}