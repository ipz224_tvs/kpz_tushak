using Lab_02_Abstract_Fabric.Devices.Interfaces;

namespace Lab_02_Abstract_Fabric.Devices.Kiaomi;

public class KiaomiNetbook : INetbook
{
    public string GetDeviceName()
    {
        return "Kiaomi Netbook";
    }
}