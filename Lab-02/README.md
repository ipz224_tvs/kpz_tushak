## Fabric Method
- Code: [here](/Lab-02/Lab_02/Lab_02_Fabric_Method)
- Class diagram: [here](/Lab-02/Lab_02/Lab_02_Fabric_Method/Lab_02_FabricMethod.drawio)
## Abstract Fabric
- Code: [here](/Lab-02/Lab_02/Lab_02_Abstract_Fabric)
- Class diagram: [here](/Lab-02/Lab_02/Lab_02_Abstract_Fabric/Lab_02_AbstractFabric.drawio)
## Sigleton
- Code: [here](/Lab-02/Lab_02/Lab_02_Singleton)
## Prototype
- Code: [here](/Lab-02/Lab_02/Lab_02_Prototype)
## Builder
- Code: [here](/Lab-02/Lab_02/Lab_02_Builder)
- Class diagram: [here](/Lab-02/Lab_02/Lab_02_Builder/Lab_02_Builder.drawio)