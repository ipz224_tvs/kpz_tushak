namespace Composite.LightHtml;

public enum DisplayType
{
    Block,
    Inline
}