using System.Text;
using Composite.LightHtml.Builder;
using Composite.LightHtml.Enums;
using Composite.LightHtml.Iterator;
using Composite.LightHtml.State;
using Composite.LightHtml.Template_Method;
using Composite.LightHtml.Visitor;

namespace Composite.LightHtml
{
    public class LightElementNode() : LightNode, IBuilder, IIterableCollection, IHtmlElement
    {
        private StringBuilder _html = null!;

        public override string OuterHtml => _html.ToString();
        public override string InnerHtml
        {
            get
            {
                var childrenHtml = new StringBuilder();
                foreach (var child in Children)
                {
                    childrenHtml.Append(child.OuterHtml);
                }
                
                return childrenHtml.ToString();
            }
        }

        private string _tagName = "";
        private DisplayType _displayType;
        private ClosingType _closingType;
        private Dictionary<string, string> _attributes = new();
        private List<string> _cssClasses = new();
        public List<LightNode> Children = new();
        
        public IBuilder AddTagName(string tagName)
        {
            _tagName = tagName;
            return this;
        }
        public IBuilder AddDisplayType(DisplayType displayType)
        {
            _displayType = displayType;
            return this;
        }
        public IBuilder AddAttributes(Dictionary<string, string> attributes)
        {
            _attributes = attributes;
            return this;
        }
        public IBuilder AddCssClasses(List<string> cssClasses)
        {
            _cssClasses = cssClasses;
            return this;
        }
        public IBuilder AddClosingType(ClosingType closingType)
        {
            _closingType = closingType;
            return this;
        }
        public IBuilder AddChildren(List<LightNode> children)
        {
            Children = children;
            return this;
        }

        public LightElementNode Build()
        {
            _html = new StringBuilder();

            if (_displayType == DisplayType.Block)
            {
                _html.Append('\n');
            }

            if (_tagName != "")
            {
                _html.Append($"<{_tagName}");
            }

            if (_attributes.Count > 0)
            {
                foreach (var attribute in _attributes)
                {
                    _html.Append($" {attribute.Key}=\"{attribute.Value}\"");
                }
            }

            if (_cssClasses.Count > 0)
            {
                _html.Append($" class=\"{string.Join(" ", _cssClasses)}\"");
            }

            if (_closingType == ClosingType.Single)
            {
                _html.Append(" />");
            }
            else if (_closingType == ClosingType.Double)
            {
                _html.Append(">");
                _html.Append(InnerHtml);

                if (_displayType == DisplayType.Block && Children.Count > 0)
                {
                    _html.Append('\n');
                }

                _html.Append($"</{_tagName}>");
            }

            return this;
        }

        //iterator
        public IIterator CreateDepthFirstIterator()
        {
            return new DepthFirstIterator(this);
        }

        public IIterator CreateBreadthFirstIterator()
        {
            return new BreadthFirstIterator(this);
        }
        
        //State
        private IElementState _elementState = null!;
        
        public void SetState(IElementState elementState)
        {
            _elementState = elementState;
        }
        public void HandleVisibility()
        {
            _elementState.HandleVisibility(this);
        }
        
        //Visitor
        public void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}