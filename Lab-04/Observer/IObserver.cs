using Observer.LightHtml;

namespace Observer;

public interface IObserver
{
    void Update(LightNode node);
}