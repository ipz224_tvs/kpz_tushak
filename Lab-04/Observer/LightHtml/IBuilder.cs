namespace Observer.LightHtml;

public interface IBuilder
{
    IBuilder AddTagName(string tagName);
    IBuilder AddDisplayType(string displayType);
    IBuilder AddClosing(bool isSingle);
    IBuilder AddCssClasses(List<string> cssClasses);
    IBuilder AddAttributes(Dictionary<string, string> attributes);
    IBuilder AddChildren(List<LightNode> children);

    LightElementNode Build();
}