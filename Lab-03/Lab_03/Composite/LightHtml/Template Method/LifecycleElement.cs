namespace Composite.LightHtml.Template_Method;

public abstract class LifecycleElement
{
    public void LifecycleMethod()
    {
        OnCreated();
        OnInserted();
        OnRemoved();
        OnStylesApplied();
        OnClassListApplied();
        OnTextRendered();
    }
    
    protected virtual void OnCreated() => Console.WriteLine("OnCreated hook called");
    protected virtual void OnInserted() => Console.WriteLine("OnInserted hook called");
    protected virtual void OnRemoved() => Console.WriteLine("OnRemoved hook called");
    protected virtual void OnStylesApplied() => Console.WriteLine("OnStylesApplied hook called");
    protected virtual void OnClassListApplied() => Console.WriteLine("OnClassListApplied hook called");
    protected virtual void OnTextRendered() => Console.WriteLine("OnTextRendered hook called");
}