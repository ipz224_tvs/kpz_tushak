namespace Memento;

public class DocumentMemento(string title, List<string> content, DateTime date) : IDocumentMemento
{
    public string Title { get; set; } = title;
    public List<string> Content { get; set; } = content;
    public DateTime Date { get; set; } = date;
}