namespace Adapter;

public class FileWriter(string filePath)
{
    public void Write(string message)
    {
        File.AppendAllText(filePath, message);
        Console.WriteLine($"{message}");
    }
    public virtual void WriteLine(string message)
    {
        File.AppendAllText(filePath, message + Environment.NewLine);
        Console.WriteLine($"{message}");
    }
}