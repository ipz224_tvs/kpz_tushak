using Lab_02_Abstract_Fabric.Devices.Interfaces;

namespace Lab_02_Abstract_Fabric.Devices.Kiaomi;

public class KiaomiSmartphone : ISmartphone
{
    public string GetDeviceName()
    {
        return "Kiaomi Smartphone";
    }
}