using Lab_02_Fabric_Method.Subscriptions;

namespace Lab_02_Fabric_Method.Factories;

public interface IPurchasePath
{
    public ISubscription CreateSubscription(string name);
}