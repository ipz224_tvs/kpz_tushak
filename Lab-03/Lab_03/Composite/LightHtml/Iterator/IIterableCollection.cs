namespace Composite.LightHtml.Iterator;

public interface IIterableCollection
{
    IIterator CreateDepthFirstIterator();
    IIterator CreateBreadthFirstIterator();
}