using Lab_01.Validators;

namespace Lab_01.Models
{
    public class Money
    {
        private int _wholePart;
        private int _restPart;
        private readonly IMoneyValidator _moneyValidator;

        public Currencies Currency { get; set; }

        public Money(int wholePart, int restPart, Currencies currency, IMoneyValidator moneyValidator)
        {
            _moneyValidator = moneyValidator;
            SetMoneyParts(wholePart, restPart);
            Currency = currency;
        }

        public void SetMoneyParts(int wholePart, int restPart)
        {
            SetWholePart(wholePart);
            SetRestPart(restPart);
        }

        private void SetWholePart(int wholePart)
        {
            _moneyValidator.ValidateWholePart(wholePart);
            _wholePart = wholePart;
        }

        private void SetRestPart(int restPart)
        {
            _moneyValidator.ValidateRestPart(restPart);
            _restPart = restPart;
        }

        public int GetWholePart()
        {
            return _wholePart;
        }

        public int GetRestPart()
        {
            return _restPart;
        }

        public decimal GetTotalMoney()
        {
            return _wholePart + _restPart * 0.01m;
        }

        public override string ToString()
        {
            return $"{GetTotalMoney()} {Currency}";
        }
    }
}