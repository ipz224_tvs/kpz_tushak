using Lab_02_Abstract_Fabric.Devices.Interfaces;

namespace Lab_02_Abstract_Fabric.Devices.IProne;

public class IProneNetbook : INetbook
{
    public string GetDeviceName()
    {
        return "IProne Netbook";
    }
}