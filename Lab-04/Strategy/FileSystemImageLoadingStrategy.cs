namespace Strategy;

using System;
using System.IO;
using System.Threading.Tasks;

public class FileSystemImageLoadingStrategy : IImageLoadingStrategy
{
    public async Task<byte[]> LoadImage(string href)
    {
        Console.WriteLine($"Loading image from file system: {href}");

        try
        {
            byte[] imageData = await File.ReadAllBytesAsync(href);
            return imageData;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error loading image from file system: {ex.Message}");
            return null!;
        }
    }
}
