namespace Composite.LightHtml.Command;

public class EditElementCommand(LightElementNode parentElementNode, LightElementNode elementToEdit, LightElementNode newElement)
    : ICommand
{
    private LightElementNode _parentElementNode = parentElementNode;
    private LightElementNode _elementToEdit = elementToEdit;
    private LightElementNode _newElement = newElement;

    public void Execute()
    {
        if (_parentElementNode.Children.Contains(_elementToEdit))
        {
            int index = _parentElementNode.Children.IndexOf(_elementToEdit);
            _parentElementNode.Children.RemoveAt(index);
            _parentElementNode.Children.Insert(index, _newElement);
        }
    }
}