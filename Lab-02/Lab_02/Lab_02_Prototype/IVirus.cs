namespace Lab_02_Prototype;

public interface IVirus
{
    public void AddChild(Virus child);
    public IVirus Clone();
}