namespace Lab_02_Prototype;

public class Virus: IVirus
{
    private double _weight;
    private int _age;
    private string _name;
    private string _type;
    public List<Virus> _children;
    
    public Virus(double weight, int age, string name, string type)
    {
        _weight = weight;
        _age = age;
        _name = name;
        _type = type;
        _children = new List<Virus>();
    }

    public void AddChild(Virus child)
    {
        _children.Add(child);
    }

    public IVirus Clone()
    {
        var clone = new Virus(_weight, _age, _name, _type);
        foreach (var child in _children)
        {
            clone.AddChild(child);
        }
        return clone;
    }
    
    public override string ToString()
    {
        var result = $" Name = {_name}, Weight = {_weight}, Age = {_age}, Type = {_type}";
        if (_children.Count != 0)
        {
            result += "\n   Children = \n";
            result = _children.Aggregate(result, (current, child) => current + ("     + " + child + "\n"));
        }

        return result;
    }
}