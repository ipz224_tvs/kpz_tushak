namespace Strategy;

using System;
using System.Net.Http;
using System.Threading.Tasks;

public class NetworkImageLoadingStrategy : IImageLoadingStrategy
{
    public async Task<byte[]> LoadImage(string href)
    {
        try
        {
            using (var client = new HttpClient())
            {
                HttpResponseMessage response = await client.GetAsync(href);
                response.EnsureSuccessStatusCode();
                byte[] imageData = await response.Content.ReadAsByteArrayAsync();
                Console.WriteLine($"Image loaded successfully from network: {href}");
                return imageData;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error loading image from network: {ex.Message}");
            return null!;
        }
    }
}
