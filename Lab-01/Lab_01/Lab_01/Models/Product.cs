using System;

namespace Lab_01.Models;

public class Product
{
    public string Name { get; set; }
    public Money Price { get; set; }
    public string MeasurementUnit { get; private set; }

    public Product(string name, Money price, string measurementUnit)
    {
        Name = name;
        Price = price;
        MeasurementUnit = measurementUnit;
    }

    public void ReducePrice(decimal amount)
    {
        decimal whole = Math.Truncate(amount);
        decimal rest = amount - whole;
        Price.SetMoneyParts(Price.GetWholePart() - (int)whole, Price.GetRestPart() - (int)(rest * 100));
    }
}
