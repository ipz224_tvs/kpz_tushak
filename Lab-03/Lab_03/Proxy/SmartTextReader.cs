namespace Proxy;

public class SmartTextReader
{
    protected char[][]? ReadText(string path)
    {
        try
        {
            return File.ReadAllLines(path)
                .Select(line => line.ToCharArray())
                .ToArray();
        }
        catch (Exception exception)
        {
            Console.WriteLine($"Error: {exception}");
            return null;
        }
    }
}