
# Principles

It is worth starting with the structure of my program. At the moment I have the following:
- **Models** folder, which includes the main classes of the program
- **Validators** folder containing validators for models
---
I will paint according to the rule:
- ***"principle - what is it? - where used?"***
---
## DRY
***"what is it?"***  
*The DRY principle emphasizes the importance of avoiding code duplication. When parts of the program code are repeated, it increases the probability of errors and makes it difficult to change.*  
  
***"where used?"***  
*It was used in all classes, but I have some doubts about **[IMoneyValidator](/Lab-01/Lab_01/Lab_01/Validators/IMoneyValidator.cs)** and **[Reporting](/Lab-01/Lab_01/Lab_01/Models/Reporting.cs)**, where in my opinion there are still similar code elements, but in fact in different contexts, than I can justify myself.*

## KISS
***"what is it?"***  
*The KISS principle calls for writing code so that it is as simple and understandable as possible. Unnecessary difficulties and complexity can lead to unpredictable problems and make it difficult to understand the program.*
  
***"where used?"***  
- ***[Money](/Lab-01/Lab_01/Lab_01/Models/Money.cs)***:
*The class represents money and performs its validation.The methods and logic of interaction with monetary units look simple and clear.*

- ***[Product](/Lab-01/Lab_01/Lab_01/Models/Product.cs)***  
*The class represents the product and has the functionality to reduce the price.The logic is simple and clear.*

- ***[Reporting](/Lab-01/Lab_01/Lab_01/Models/Reporting.cs)***  
*The class is used to create various reports. The methods for logging events and printing reports are fairly simple and straightforward.*

- ***[Warehouse](/Lab-01/Lab_01/Lab_01/Models/Warehouse.cs)***  
*The class represents the warehouse and performs the logic of buying and importing goods.*

## SOLID
***"what is it?"*** 

*Is an abbreviation that covers five main object-oriented principles:
- **Single Responsibility Principle**: Each class must have only one reason to change.
- **Open/Closed Principle**: Software entities (classes, modules, etc.) must be open for expansion, but closed for modification.
- **Liskov Substitution Principle**: Objects of the base class should be replaced by their derivatives without losing the correctness of the program.
- **Interface Segregation Principle**: Clients should not depend on interfaces they do not use.
- **Dependency Inversion Principle**: Upper-level modules should not be dependent on lower-level modules. Both types of modules must depend on abstractions.*  

***"where used?"***  
As for SOLID, I used:
- **Single Responsibility Principle**  
*Examples:*

  - ***[MoneyValidator](/Lab-01/Lab_01/Lab_01/Validators/MoneyValidator.cs)***: Responsible only for validation of money.
  - ***[Money](/Lab-01/Lab_01/Lab_01/Models/Money.cs)***: Represents money and performs its validation.
  - ***[Product](/Lab-01/Lab_01/Lab_01/Models/Product.cs)***: Represents the product and has the functionality to reduce the price.
  - ***[Reporting](/Lab-01/Lab_01/Lab_01/Models/Reporting.cs)***: Used to create various reports.
  - ***[Warehouse](/Lab-01/Lab_01/Lab_01/Models/Warehouse.cs)***: Represents the warehouse and performs the logic of buying and importing goods.

- **Open/Closed Principle**:  
*Examples:*
  - ***[MoneyValidator](/Lab-01/Lab_01/Lab_01/Validators/MoneyValidator.cs)***: It is easily possible to extend to add new validators without changing the existing code.
  - ***[Money](/Lab-01/Lab_01/Lab_01/Models/Money.cs)***: Open for expansion (you can add new methods or properties).
  - ***[Product](/Lab-01/Lab_01/Lab_01/Models/Product.cs)***: Methods can be extended for new types of actions without changing the existing code.
  - ***[Reporting](/Lab-01/Lab_01/Lab_01/Models/Reporting.cs)***: Event logging and report printing methods can be expanded.
  - ***[Warehouse](/Lab-01/Lab_01/Lab_01/Models/Warehouse.cs)***: It is easily possible to extend to add new functionality.


## YAGNI
***"what is it?"***  
*The YAGNI principle indicates that you do not need to add functionality to an application that is not a current requirement. It aims to avoid unnecessary code and maintain the simplicity of the program.*
  
***"where used?"***  
*If we talk about this principle, then it was used everywhere, since there were no implementations other than those prescribed in the task.*
## Composition Over Inheritance
***"what is it?"***  
*This principle emphasizes the importance of using object composition instead of inheritance. The composition provides greater flexibility and avoids problems associated with unnecessary or unnecessary methods of inheritance.*
  
***"where used?"***  
*All classes: they use composition and aggregation, which is a positive approach.*

## Program to Interfaces not Implementations
***"what is it?"***  
*The principle indicates that the program should cooperate with interfaces, not specific implementations. This provides flexibility and facilitates exchangeability of implementations without changing client code.*
  
***"where used?"***  
*Speaking about this principle, I found its implementation only within the framework of valdators, which gave me more flexibility than with conventional implementation. Therefore, it is worth leaving these links here:*
- ***[Money](/Lab-01/Lab_01/Lab_01/Models/Money.cs)***
- ***[Warehouse](/Lab-01/Lab_01/Lab_01/Models/Warehouse.cs)***

*Well, the interfaces themselves and their implementation:*
- ***[IMoneyValidator](/Lab-01/Lab_01/Lab_01/Validators/IMoneyValidator.cs)***
- ***[MoneyValidator](/Lab-01/Lab_01/Lab_01/Validators/MoneyValidator.cs)***
- ***[IWarehouseValidator](/Lab-01/Lab_01/Lab_01/Validators/IWarehouseValidator.cs)***
- ***[WarehouseValidator](/Lab-01/Lab_01/Lab_01/Validators/IWarehouseValidator.cs)***


## Fail Fast
***"what is it?"***  
*This principle states that the program must stop immediately (issue an error or throw an exception) when an error or incorrect conditions are detected. This helps to quickly identify and eliminate problems, which increases the stability of the program.*
  
***"where used?"***  
*Like YAGNI, this principle is used everywhere, since it is inappropriate to continue the program as a whole if an error occurs in one of the aspects.*