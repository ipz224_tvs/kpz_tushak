namespace Memento;

public class TextDocument(string title, List<string> content)
{
    private string Title { get; set; } = title;
    public List<string> Content { get; set; } = content;
    
    public void AddContent(string content)
    {
        Content.Add(content);
    }

    public IDocumentMemento CreateMemento()
    {
        return new DocumentMemento(Title, Content, DateTime.Now);
    }

    public void RestoreMemento(IDocumentMemento memento)
    {
        if (memento != null)
        {
            Title = memento.Title;
            Content = memento.Content;
        }
    }

    public override string ToString()
    {
        return $"\nTitle: {Title},\nContent:\n{string.Join("\n", Content)}\n\n";
    }
}