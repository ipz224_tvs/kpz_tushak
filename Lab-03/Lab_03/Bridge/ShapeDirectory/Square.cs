using Bridge.RenderWays;

namespace Bridge.ShapeDirectory;

public class Square: Shape
{
    public Square(IRenderer renderer) : base(renderer)
    {
    }

    public override void Draw()
    {
        Renderer.Render("Square");
    }
}