﻿using Mediator;

CommandCentre commandCentre = new CommandCentre();

Aircraft aircraft1 = new Aircraft("Boeing 747");
Aircraft aircraft2 = new Aircraft("Airbus A320");
Aircraft aircraft3 = new Aircraft("Airbus A330");

Runway runway1 = new Runway();
Runway runway2 = new Runway();

commandCentre.AddRunway(runway1);
commandCentre.AddRunway(runway2);

commandCentre.RequestRunway(aircraft1); // good
Console.WriteLine();

commandCentre.RequestRunway(aircraft2);  // good
Console.WriteLine();

commandCentre.RequestRunway(aircraft3);  // wait
Console.WriteLine();

commandCentre.RequestTakeOff(aircraft1); // good
Console.WriteLine();

commandCentre.RequestTakeOff(aircraft2); // good
Console.WriteLine();

commandCentre.RequestTakeOff(aircraft3); //doesn't have a runway
Console.WriteLine();

commandCentre.RequestRunway(aircraft3); // good
Console.WriteLine();

commandCentre.RequestTakeOff(aircraft3); // good
Console.WriteLine();

