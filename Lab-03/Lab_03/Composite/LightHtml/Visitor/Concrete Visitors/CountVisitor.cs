namespace Composite.LightHtml.Visitor.Concrete_Visitors;

public class CountVisitor : IVisitor
{
    private static int _count;
    public void Visit(LightElementNode node)
    {
        _count = 0;
        foreach (var child in node.Children)
        {
            _count++;
        }
        
        Console.WriteLine($"Element {nameof(node)} has {_count} children");
        Console.WriteLine("Count visitor end\n");
    }
}