using System.Text.RegularExpressions;

namespace Proxy;

public class SmartTextReaderLocker : SmartTextReader
{
    private readonly Regex _regex;

    public SmartTextReaderLocker(string pattern)
    {
        _regex = new Regex(pattern);
    }

    public new void ReadText(string path)
    {
        if (!_regex.IsMatch(path))
        {
            base.ReadText(path);
            return;
        }

        Console.WriteLine("Access denied!");
    }
}