using Lab_02_Abstract_Fabric.Devices.Interfaces;

namespace Lab_02_Abstract_Fabric.Devices.Kiaomi;

public class KiaomiEBook : IEBook
{
    public string GetDeviceName()
    {
        return "Kiaomi EBook";
    }
}