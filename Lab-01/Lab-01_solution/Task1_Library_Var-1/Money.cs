using Task1_Library_Var;

public class Money: ErrorNotifier
{
    private int _intPart;
    private int _decimalPart;
    private string _currency;


    public Money(int intPart, int decimalPart, string currency)
    {
        _intPart = intPart;
        _decimalPart = decimalPart;
        _currency = currency;
    }

    public string GetAmount()
    {
        return $"{_intPart}.{_decimalPart} {_currency}";
    }

  

    public void SetCurrency(string currency)
    {
        _currency = currency;
    }

    private bool IsValidNonNegative(int value)
    {
        return value >= 0;
    }

    private bool IsValidDecimalPart(int decimalPart)
    {
        return IsValidNonNegative(decimalPart) && decimalPart < 100;
    }
}