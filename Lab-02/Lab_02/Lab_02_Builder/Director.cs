using System.Drawing;
using Lab_02_Builder.Builders;

namespace Lab_02_Builder;

public class Director
{
    private ICharacterBuilder _builder;

    public ICharacterBuilder Builder
    {
        set => _builder = value;
    }
    
    public void CreateCharacter(string name, string classname, string gender, int age,  Color hairColor, Color eyeColor,
        string bodyType, string armor, string weapon, Dictionary<string, bool> tasks, Dictionary<string, int> inventoryItems)
    {
        _builder
            .SetName(name)
            .SetClass(classname)
            .SetGender(gender)
            .SetAge(age)
            .SetBodyType(bodyType)
            .SetHairColor(hairColor)
            .SetEyeColor(eyeColor)
            .SetArmor(armor)
            .SetWeapon(weapon)
            .SetToDoList(tasks)
            .SetInventoryItems(inventoryItems);
    }
}