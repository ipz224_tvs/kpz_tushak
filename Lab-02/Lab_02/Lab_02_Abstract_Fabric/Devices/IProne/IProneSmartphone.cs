using Lab_02_Abstract_Fabric.Devices.Interfaces;

namespace Lab_02_Abstract_Fabric.Devices.IProne;

public class IProneSmartphone : ISmartphone
{
    public string GetDeviceName()
    {
        return "IProne SmartPhone";
    }
}