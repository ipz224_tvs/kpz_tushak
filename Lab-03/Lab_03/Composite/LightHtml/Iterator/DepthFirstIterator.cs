namespace Composite.LightHtml.Iterator
{
    public class DepthFirstIterator : IIterator
    {
        private readonly Stack<LightNode> _stack = new();

        public DepthFirstIterator(LightNode root)
        {
            _stack.Push(root);
        }
        public bool HasNext() => _stack.Count > 0;

        public LightNode Next()
        {
            if (!HasNext())
                throw new InvalidOperationException("No more elements to iterate");
            
            LightNode node = _stack.Pop();
            if (node is LightElementNode elementNode)
                for (int i = elementNode.Children.Count - 1; i >= 0; i--)
                    _stack.Push(elementNode.Children[i]);
            
            return node;
        }
    }
}