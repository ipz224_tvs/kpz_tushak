using Lab_02_Abstract_Fabric.Devices.Interfaces;
using Lab_02_Abstract_Fabric.Devices.IProne;

namespace Lab_02_Abstract_Fabric.Factories;

public class IProneFactory: ITechnologyFactory
{
    public IEBook CreateEBook()
    {
        return new IProneEBook();
    }

    public ILaptop CreateLaptop()
    {
        return new IProneLaptop();
    }

    public INetbook CreateNetbook()
    {
        return new IProneNetbook();
    }

    public ISmartphone CreateSmartphone()
    {
        return new IProneSmartphone();
    }
}