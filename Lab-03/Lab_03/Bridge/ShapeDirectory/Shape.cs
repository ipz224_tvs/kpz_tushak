using Bridge.RenderWays;

namespace Bridge.ShapeDirectory;

public abstract class Shape
{
    protected IRenderer Renderer;

    protected Shape(IRenderer renderer)
    {
        Renderer = renderer;
    }

    public abstract void Draw();
}