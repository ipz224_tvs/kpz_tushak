﻿using Composite.LightHtml;
using Composite.LightHtml.Command;
using Composite.LightHtml.Enums;
using Composite.LightHtml.Iterator;
using Composite.LightHtml.State;
using Composite.LightHtml.Visitor.Concrete_Visitors;

var titleTextNode = new LightTextNode("Welcome to My Website");
var titleElement = new LightElementNode()
    .AddTagName("h1")
    .AddDisplayType(DisplayType.Block)
    .AddClosingType(ClosingType.Double)
    .AddAttributes(new Dictionary<string, string> { { "id", "title" } })
    .AddCssClasses(["title"])
    .AddChildren([titleTextNode])
    .Build();

var paragraphTextNode = new LightTextNode("This is the main content of the website");
var paragraphElement = new LightElementNode()
    .AddTagName("p")
    .AddDisplayType(DisplayType.Block)
    .AddClosingType(ClosingType.Double)
    .AddAttributes(new Dictionary<string, string> { { "id", "paragraph" } })
    .AddCssClasses(["paragraph"])
    .AddChildren([paragraphTextNode])
    .Build();

var headerElement = new LightElementNode()
    .AddTagName("header")
    .AddDisplayType(DisplayType.Block)
    .AddClosingType(ClosingType.Double)
    .AddAttributes(new Dictionary<string, string> { { "id", "header" } })
    .AddCssClasses(["header"])
    .AddChildren([titleElement])
    .Build();

var mainElement = new LightElementNode()
    .AddTagName("main")
    .AddDisplayType(DisplayType.Block)
    .AddClosingType(ClosingType.Double)
    .AddAttributes(new Dictionary<string, string> { { "id", "content" } })
    .AddCssClasses(["content"])
    .AddChildren([paragraphElement])
    .Build();

var rootElement = new LightElementNode()
    .AddTagName("div")
    .AddDisplayType(DisplayType.Block)
    .AddClosingType(ClosingType.Double)
    .AddAttributes(new Dictionary<string, string> { { "id", "root" } })
    .AddCssClasses(["container", "main"])
    .AddChildren([headerElement])
    .Build();

#region Iterator
/*var html = rootElement.OuterHtml;
//Console.WriteLine(html);

var depthFirstIterator = rootElement.CreateDepthFirstIterator();
while (depthFirstIterator.HasNext())
{
    Console.WriteLine(depthFirstIterator.Next());
}

var breadthFirstIterator = rootElement.CreateBreadthFirstIterator();
while (breadthFirstIterator.HasNext())
{
    Console.WriteLine(breadthFirstIterator.Next());
}*/
#endregion

#region Command
var parentElement = rootElement;
var elementToAddAndDelete = mainElement;
/*var newElement = new LightElementNode()
    .AddTagName("div")
    .AddDisplayType(DisplayType.Block)
    .AddClosingType(ClosingType.Double)
    .AddAttributes(new Dictionary<string, string> { { "id", "content" } })
    .AddCssClasses(["content"])
    .AddChildren([new LightTextNode("This is the content of the new element")])
    .Build(); */

var addCommand = new AddElementCommand(parentElement, elementToAddAndDelete);
/*var editCommand = new EditElementCommand(parentElement, elementToAddAndDelete, newElement);
var removeCommand = new RemoveElementCommand(parentElement, newElement);*/

addCommand.Execute();
/*Console.WriteLine(rootElement.Build().OuterHtml);
Console.WriteLine("===============================================================\n");

editCommand.Execute();
Console.WriteLine(rootElement.Build().OuterHtml);
Console.WriteLine("===============================================================\n");

removeCommand.Execute();
Console.WriteLine(rootElement.Build().OuterHtml);
Console.WriteLine("===============================================================\n");*/

#endregion

#region State

/*//green
mainElement.SetState(new VisibleElementState());
mainElement.HandleVisibility();

//red
mainElement.SetState(new HiddenElementState());
mainElement.HandleVisibility();

//blue
mainElement.SetState(new DisabledElementState());
mainElement.HandleVisibility();*/

#endregion

#region Visitor
/*var styleVisitor = new StyleVisitor();
rootElement.Accept(styleVisitor);

var countVisitor = new CountVisitor();
rootElement.Accept(countVisitor);*/

#endregion